## Installation

```{r}
# Make sure you have 'libgit2' installed
stopifnot(git2r::libgit2_features()$ssh)
devtools::install_git("git@gricad-gitlab.univ-grenoble-alpes.fr:lucah/hadaca_scoring.git", subdir = "pkg")
```

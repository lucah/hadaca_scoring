################################################################################

#' Mean absolute error (MAE) between two matrices.
#'
#' @param M1 A matrix.
#' @param M2 A matrix of same dimension as `M1`.
#'
#' @name MAE-RMSE
#' @export
MAE <- function(M1, M2) {
  mean(abs(M1 - M2))
}

#' Root mean squared error (RMSE) between two matrices.
#'
#' @rdname MAE-RMSE
#' @export
RMSE <- function(M1, M2) {
  sqrt(mean((M1 - M2)^2))
}

################################################################################

#' Compare the A matrices
#'
#' This function compute errors between the two A matrices,
#'   after guessing the permutation.
#'
#' @param A_reel The control A matrix.
#' @param A_estim The calculated A matrix.
#'   Permutation is guessed from the minimum MAE.
#'
#' @export
compare_A_matrix <- function(A_reel, A_estim) {

  N <- ncol(A_reel)
  K <- nrow(A_reel)
  stopifnot(K > 1)

  stopifnot(ncol(A_estim) == N)
  stopifnot(nrow(A_estim) < ncol(A_estim))
  stopifnot(nrow(A_estim) <= 10)
  stopifnot(!anyNA(A_estim))

  # if not supplying enough types (make sure that {nrow(A_estim) >= K})
  if (nrow(A_estim) < K) A_estim <- rbind(A_estim, matrix(0, K - nrow(A_estim), N))

  comb <- unlist(
    combinat::combn(nrow(A_estim), K, fun = combinat::permn, simplify = FALSE),
    recursive = FALSE
  )
  comb_MAE <- sapply(comb, function(perm) {
    MAE(A_reel, A_estim[perm, , drop = FALSE])
  })

  perm <- comb[[which.min(comb_MAE)]]
  A_estim_perm <- A_estim[perm, , drop = FALSE]

  list(
    perm = perm,
    MAE  = MAE(A_reel, A_estim_perm),
    RMSE = RMSE(A_reel, A_estim_perm)
  )
}


#' Compare the D matrices
#'
#' This function compute errors between the two D matrices.
#'
#' @param D_reel The control D matrix.
#' @param D_estim The calculated D matrix.
#'
#' @export
compare_D_matrix <- function(D_reel, D_estim) {
  list(RMSE = RMSE(D_reel, D_estim))
}

################################################################################
